from __future__ import unicode_literals

from django.db import models


# Create your models here.
class BotUser(models.Model):
    username = models.CharField(max_length=255, default='', null=True)
    first_name = models.CharField(max_length=255, default='', null=True)
    last_name = models.CharField(max_length=255, default='', null=True)
    chat_id = models.IntegerField(unique=True)
    is_admin = models.BooleanField(default=False)
    cmd_status = models.IntegerField(default=0)
    cmd_params = models.CharField(max_length=300, default='')
    last_query = models.CharField(max_length=1024, default='')


class BookCategory(models.Model):
    name = models.CharField(max_length=255, null=True, default=-1)
    local_id = models.IntegerField(null=True, default=-1)
    parent_id = models.IntegerField(null=True, default=-1)


class Book(models.Model):
    name = models.CharField(max_length=255, null=True, default='')
    author = models.CharField(max_length=255, null=True, default='')
    url = models.CharField(max_length=1024, null=True, default='')
    picture = models.CharField(max_length=1024, null=True, default='')
    category = models.ForeignKey(BookCategory, on_delete=models.CASCADE, null=True)
    price = models.FloatField(null=True, default=0)
    available = models.BooleanField(default=True)
    visible = models.BooleanField(default=False)
    local_id = models.IntegerField(default=-1)
    query_data = models.CharField(max_length=510, null=True, default='')
    description = models.CharField(max_length=1024, null=True, default='')


class BookUserRating(models.Model):
    book = models.ForeignKey(Book, on_delete=models.CASCADE, null=True)
    user = models.ForeignKey(BotUser, on_delete=models.CASCADE, null=True)
    rating = models.IntegerField(null=False, default=0)


class Settings(models.Model):
    parameter = models.CharField(max_length=255, null=False)
    value = models.CharField(max_length=255, null=False)
