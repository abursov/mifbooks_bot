# -*- coding: utf8 -*-

import json
import shlex
import time
import urllib
import telepot
import xmltodict
import logging
import hashlib
from django.conf import settings
from django.http import HttpResponseForbidden, HttpResponseBadRequest, JsonResponse
from django.template.loader import render_to_string
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View
from telepot.namedtuple import ReplyKeyboardMarkup, KeyboardButton, InlineKeyboardMarkup, InlineKeyboardButton
from .models import BookCategory, Book, BotUser, Settings, BookUserRating

CMD_NOT_FOUND = 'Команда не найдена'
NOT_FOUND = 'Ничего не найдено'
DATE_FORMAT = '%d.%m.%Y'
# Важно! Оставить в таком виде, иначе метод .get() не находит строку в списке
CMD_SEARCH = u'Поиск'
CMD_CATEGORIES = u'Категории'
UPDATE_DATE_PARAMETER = 'update_date'
UPDATE_HASH_PARAMETER = 'update_hash_data'

TelegramBot = telepot.Bot(settings.TELEGRAM_BOT_TOKEN)
markup_default = ReplyKeyboardMarkup(keyboard=[[KeyboardButton(text=CMD_SEARCH),
                                                KeyboardButton(text=CMD_CATEGORIES)], ],
                                     resize_keyboard=True)

keyboard_default = markup_default
inline_keyboard = InlineKeyboardMarkup()


# Функция-wrapper для обработки запросов к боту
class CommandReceiveView(View):
    def post(self, request, bot_token):
        if bot_token != settings.TELEGRAM_BOT_TOKEN:
            return HttpResponseForbidden('Invalid token')

        raw = request.body
        try:
            payload = json.loads(raw)
        except ValueError:
            return HttpResponseBadRequest('Invalid request body')

        try:
            # Проверить это сообщение или callback-запрос
            if payload.get('message') is not None:
                bot_user = user_handle(payload['message'])
                if 0 == bot_user.cmd_status:
                    handle_cmd(bot_user, payload['message'])

            if payload.get('callback_query') is not None:
                bot_user = user_handle(payload['callback_query']['message'])
                handle_callback(bot_user, payload['callback_query'])

            # Запустить проверку обновления
            handle_update()
        finally:
            # Возвращать всегда, иначе Telegram webhook начинает сходить с ума
            return JsonResponse({}, status=200)

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(CommandReceiveView, self).dispatch(request, *args, **kwargs)


def user_handle(msg):
    # Найти пользователя по ID-чата или создать его
    try:
        bot_user = BotUser.objects.get(chat_id=msg['chat']['id'])
    except BotUser.DoesNotExist:
        bot_user = BotUser.objects.create(chat_id=msg['chat']['id'],
                                          username=msg['chat'].get('username'),
                                          first_name=msg['chat'].get('first_name'),
                                          last_name=msg['chat'].get('last_name'))
        bot_user.save()
    return bot_user


# Обработчик текстовых команд
def handle_cmd(bot_user, msg):
    commands = {
        # Показать стартовое сообщение, помощь
        '/start': _display_help,
        '/help': _display_help,
        # Показать поиск
        CMD_SEARCH.lower(): _display_search,
        # Показать категории
        CMD_CATEGORIES.lower(): _display_categories,
    }

    cmd = msg.get('text')  # command

    if cmd is not None:

        # Найти функцию в списке выше и вызвать ее
        func = commands.get(cmd.split()[0].lower())
        if func is not None:
            text = func(bot_user, msg)
            # В функциях меняется раскладка по-умолчанию, нужно перевызвать ее и обновить
            global keyboard_default
            _send_message(bot_user.chat_id, text, parse_mode='Markdown', reply_markup=keyboard_default)
            keyboard_default = markup_default
        else:
            # Если функция не найдена, попробовать поискать запрос по базе
            _handle_search(bot_user, msg)

    return True


# Обработчик callback-запоросов (кнопки показать еще и т.д.)
# Работает аналогично handle_cmd
def handle_callback(chat_user=None, msg=None):
    query_id, from_id, query_data = telepot.glance(msg, flavor='callback_query')

    call_back_commands = {
        'show_next': _callback_show_next,
        'show_categories': _callback_show_categories,
        'rate_book': _callback_rate_book,
        'show_search': _callback_show_search,
    }

    cmd = query_data.split()

    func = call_back_commands.get(cmd[0].lower())
    if func:
        return func(chat_user, query_data, msg)
    return False


# Функция отправки сообщений
def _send_message(chat_id, text, parse_mode=None, reply_markup=None):
    if text is None or text == '' or len(text) > 2000:
        return False
    try:
        return TelegramBot.sendMessage(chat_id=chat_id, text=text, parse_mode=parse_mode, reply_markup=reply_markup)
    except telepot.exception.TelegramError:
        return False


# Редактирование сообщений (нужно при клике на кнопку "показать еще")
def _edit_text_message(msg_identifier, text, parse_mode=None, reply_markup=None):
    try:
        TelegramBot.editMessageText(msg_identifier, text,
                                    parse_mode=parse_mode,
                                    reply_markup=reply_markup)
    except telepot.exception.TelegramError as ex:
        return False
    return True


# Показать помощь
def _display_help(bot_user, msg=None):
    render_str = render_to_string('help.md', {'first_name': bot_user.first_name})
    return render_str


# Показать сообщение о поиске
def _display_search(bot_user=None, cmd=None, msg=None):
    render_str = render_to_string('search_message.md', {})
    return render_str


# Проверить верное ли количество параметров передано в функцию
def _is_correct_cmd(cmd=None, length=2):
    if cmd is None:
        return False
    if len(cmd) < length:
        return False
    return True


# Когда re не работает, за дело берутся они!
def replace_insensitive(string, target):
    no_case = string.lower()
    index = no_case.find(target.lower())
    if index > -1:
        result = string[:index] + '*' + string[index:index + len(target)] + '*' + string[index + len(target):]
        return result
    else:
        return string


# Поиск по книгам
def _search(bot_user, msg, offset=0, category_id=None, search_type=0):
    books = None
    search_query = ''
    if category_id is not None:
        category = BookCategory.objects.get(local_id=category_id)
        books = Book.objects.filter(category=category, visible=True).order_by('name')
        template_file = 'category_not_found.md'
        book_file = 'book.md'
        category_name = category.name
    else:
        search_query = bot_user.last_query
        if search_type == 0:
            books = Book.objects.filter(description__icontains=search_query, visible=True).order_by('name')
        elif search_type == 1:
            books = Book.objects.filter(author__icontains=search_query, visible=True).order_by('author', 'name')
        elif search_type == 2:
            books = Book.objects.filter(name__icontains=search_query, visible=True).order_by('name')

        template_file = 'books_not_found.md'
        book_file = 'book_found.md'
        category_name = ''

    offset_value = int(offset)

    if books.count() == 0:
        render_str = render_to_string(template_file, {'query': bot_user.last_query})

        try:
            _edit_text_message(msg_identifier=telepot.message_identifier(msg['message']),
                               text=render_str,
                               parse_mode='Markdown')
        except KeyError:
            _send_message(bot_user.chat_id,
                          text=render_str,
                          parse_mode='Markdown')
        return False
    else:

        if offset_value < 0:
            offset_value = books.count() - 1
        else:
            if offset_value > books.count() - 1:
                offset_value = 0
        book = books[int(offset_value)]

        if category_id is None:
            if search_type == 0:
                book.description = replace_insensitive(book.description, search_query)
            elif search_type == 1:
                book.author = replace_insensitive(book.author, search_query)

        render_str = render_to_string(book_file, {'name': book.name,
                                                  'author': book.author,
                                                  'price': int(book.price),
                                                  'url': book.url,
                                                  'picture': book.picture,
                                                  'description': book.description,
                                                  'index': offset_value + 1,
                                                  'count': books.count(),
                                                  'query': bot_user.last_query,
                                                  'rating': _get_book_rating(book),
                                                  'category': category_name, })

        if books.count() == 1:
            markup = InlineKeyboardMarkup(inline_keyboard=[
                [InlineKeyboardButton(text='Открыть на сайте МИФ', url=book.url)],
            ])
        else:

            if category_id is not None:
                markup = InlineKeyboardMarkup(inline_keyboard=[
                    [
                        InlineKeyboardButton(text='Открыть на сайте МИФ', url=book.url),
                        InlineKeyboardButton(text='Поставить оценку',
                                             callback_data='rate_book %d %d 0 %d %d' % (
                                                 book.id, category_id, offset_value + 1, books.count()
                                             )),

                    ],
                    [
                        InlineKeyboardButton(text='Предыдущая',
                                             callback_data='show_categories %d %d' % (offset_value - 1, category_id)),
                        InlineKeyboardButton(text='Следующая',
                                             callback_data='show_categories %d %d' % (offset_value + 1, category_id))
                    ],
                ])
            else:
                markup = InlineKeyboardMarkup(inline_keyboard=[
                    [
                        InlineKeyboardButton(text='Открыть на сайте МИФ', url=book.url),
                        InlineKeyboardButton(text='Поставить оценку',
                                             callback_data='rate_book %d 0 0 %d %d' % (
                                                 book.id, offset_value + 1, books.count())),
                    ],
                    [
                        InlineKeyboardButton(text='Предыдущая',
                                             callback_data='show_next %d %d' % (offset_value - 1, search_type)),
                        InlineKeyboardButton(text='Следующая',
                                             callback_data='show_next %d %d' % (offset_value + 1, search_type))
                    ],
                ])

        global inline_keyboard
        inline_keyboard = markup

        try:
            _edit_text_message(msg_identifier=telepot.message_identifier(msg['message']),
                               text=render_str,
                               parse_mode='Markdown', reply_markup=markup)
        except KeyError:
            _send_message(bot_user.chat_id,
                          text=render_str,
                          parse_mode='Markdown', reply_markup=markup)

    return True


# Подсчет пользовательского рейтинга книги
def _get_book_rating(book):
    rating = BookUserRating.objects.filter(rating__gt=0, book__id=book.id)
    if rating.count() == 0:
        return 'Не известен'

    rating_sum = 0.0
    for val in rating:
        rating_sum += val.rating

    return '%g/5' % (rating_sum / rating.count())


# Обработчик запроса поиска по базе (выбор типа поиска)
def _handle_search(bot_user, msg=None):
    if msg is None:
        return False

    bot_user.last_query = msg.get('text')
    bot_user.save()

    markup = InlineKeyboardMarkup(inline_keyboard=[
        [InlineKeyboardButton(text='По названию', callback_data='show_search %d' % 2), ],
        [InlineKeyboardButton(text='По автору', callback_data='show_search %d' % 1), ],
        [InlineKeyboardButton(text='По аннотации', callback_data='show_search %d' % 0), ],
    ])
    _send_message(bot_user.chat_id,
                  text='Выберите тип поиска',
                  parse_mode='Markdown', reply_markup=markup)

    return True


# Вывод следующих книг по смещению
def _callback_show_next(bot_user=None, cmd=None, msg=None):
    if cmd is None:
        return None

    try:
        command_param = shlex.split(cmd)
    except ValueError:
        return NOT_FOUND

    if _is_correct_cmd(command_param, 3) is False:
        return NOT_FOUND

    _search(bot_user, msg, offset=int(command_param[1]), search_type=int(command_param[2]))

    return True


# Показать список категорий
def _display_categories(bot_user=None, cmd=None, msg=None):
    if cmd is None:
        return None

    categories = BookCategory.objects.filter(parent_id=1)
    keyboard = []
    for cat in categories:
        keyboard.append([InlineKeyboardButton(text=cat.name, callback_data='show_categories 0 %d' % cat.local_id)])

    global keyboard_default
    keyboard_default = InlineKeyboardMarkup(inline_keyboard=keyboard)
    render_str = render_to_string('category_choose.md', {})

    return render_str


# Показать книги в категории
def _callback_show_categories(bot_user=None, cmd=None, msg=None):
    if cmd is None:
        return None

    try:
        command_param = shlex.split(cmd)
    except ValueError:
        return NOT_FOUND

    if _is_correct_cmd(command_param, 3) is False:
        return NOT_FOUND

    if int(command_param[2]) == 0:

        categories = BookCategory.objects.filter(parent_id=1)
        keyboard = []
        for cat in categories:
            keyboard.append([InlineKeyboardButton(text=cat.name, callback_data='show_categories 0 %d' % cat.local_id)])

        markup = InlineKeyboardMarkup(inline_keyboard=keyboard)
        render_str = render_to_string('category_choose.md', {})
        _edit_text_message(msg_identifier=telepot.message_identifier(msg['message']),
                           text=render_str,
                           parse_mode='Markdown', reply_markup=markup)
    else:
        _search(bot_user, msg, offset=int(command_param[1]), category_id=int(command_param[2]))

    return True


# Поставить оценку книге
def _callback_rate_book(bot_user=None, cmd=None, msg=None):
    if cmd is None:
        return None

    try:
        command_param = shlex.split(cmd)
    except ValueError:
        return NOT_FOUND

    if _is_correct_cmd(command_param, 6) is False:
        return NOT_FOUND

    book_id = int(command_param[1])
    category_id = int(command_param[2])
    rating = int(command_param[3])
    index = int(command_param[4])
    count = int(command_param[5])

    if category_id > 0:
        book_file = 'book.md'
        category = BookCategory.objects.get(local_id=category_id)
        category_name = category.name

    else:
        book_file = 'book_found.md'
        category_name = ''

    book = Book.objects.get(id=book_id)

    if rating == 0:
        markup = InlineKeyboardMarkup(inline_keyboard=[
            [
                InlineKeyboardButton(text=u'\U00002B50',
                                     callback_data='rate_book %d %d 1 %d %d' % (
                                         book.id, category_id, index, count
                                     )),
            ],
            [
                InlineKeyboardButton(text=u'\U00002B50' + u'\U00002B50',
                                     callback_data='rate_book %d %d 2 %d %d' % (
                                         book.id, category_id, index, count
                                     )),
            ],
            [
                InlineKeyboardButton(text=u'\U00002B50' + u'\U00002B50' + u'\U00002B50',
                                     callback_data='rate_book %d %d 3 %d %d' % (
                                         book.id, category_id, index, count
                                     )),
            ],
            [
                InlineKeyboardButton(text=u'\U00002B50' + u'\U00002B50' + u'\U00002B50' + u'\U00002B50',
                                     callback_data='rate_book %d %d 4 %d %d' % (
                                         book.id, category_id, index, count
                                     )),
            ],
            [
                InlineKeyboardButton(text=u'\U00002B50' + u'\U00002B50' + u'\U00002B50' + u'\U00002B50' + u'\U00002B50',
                                     callback_data='rate_book %d %d 5 %d %d' % (
                                         book.id, category_id, index, count
                                     )),
            ],
            [
                InlineKeyboardButton(text='Отмена',
                                     callback_data='rate_book %d %d -1 %d %d' % (
                                         book.id, category_id, index, count
                                     )),
            ],
        ])
    else:
        global inline_keyboard
        markup = inline_keyboard
        if rating > 0:
            rating_data, created = BookUserRating.objects.get_or_create(
                book=book,
                user=bot_user)
            rating_data.rating = rating
            rating_data.save()

    render_str = render_to_string(book_file, {'name': book.name,
                                              'author': book.author,
                                              'price': int(book.price),
                                              'url': book.url,
                                              'picture': book.picture,
                                              'description': book.description,
                                              'index': index,
                                              'count': count,
                                              'query': bot_user.last_query,
                                              'rating': _get_book_rating(book),
                                              'category': category_name, })

    _edit_text_message(msg_identifier=telepot.message_identifier(msg['message']),
                       text=render_str,
                       parse_mode='Markdown', reply_markup=markup)


# Начать поиск по выбранному типу
def _callback_show_search(bot_user=None, cmd=None, msg=None):
    if cmd is None:
        return None

    try:
        command_param = shlex.split(cmd)
    except ValueError:
        return NOT_FOUND

    if _is_correct_cmd(command_param) is False:
        return NOT_FOUND

    _search(bot_user, msg, search_type=int(command_param[1]))

    return True


# Синхронизация базы данных
def sync_db(bot_user=None, msg=None):
    # В production необходимы права на запись для пользователя nobody
    xml_local = 'catalog.xml'

    try:
        update_hash_data = Settings.objects.get(parameter=UPDATE_HASH_PARAMETER)
    except Settings.DoesNotExist:
        update_hash_data = Settings.objects.create(parameter=UPDATE_HASH_PARAMETER, value='')

    local_hash = hashlib.md5(open(xml_local, 'rb').read()).hexdigest()

    if local_hash == update_hash_data.value:
        return False

    try:
        with open(xml_local) as fd:
            doc = xmltodict.parse(fd.read())
    except IOError:
        return False

    data = json.loads(json.dumps(doc))

    # Обработка категорий, обновление описания, индекса родительского элемента
    categories = data['yml_catalog']['shop']['categories']['category']
    for cat in categories:
        try:
            category = BookCategory.objects.get(local_id=cat['@id'])
        except BookCategory.DoesNotExist:
            category = BookCategory.objects.create()

        category.name = cat['#text']
        if '@parentId' in cat:
            category.parent_id = int(cat['@parentId'])
        category.local_id = int(cat['@id'])
        category.save()

    # Убрать все книги из поиска на время обновления
    Book.objects.all().update(visible=False)

    # Обраточка отдельных книг
    offers = data['yml_catalog']['shop']['offers']['offer']

    for off in offers:
        try:
            offer = Book.objects.get(local_id=off['@id'])
        except Book.DoesNotExist:
            # Присвоение категории
            if 'categoryId' in off:
                try:
                    category = BookCategory.objects.get(local_id=off['categoryId'])
                except BookCategory.DoesNotExist:
                    category = None
            else:
                category = None

            offer = Book.objects.create(category=category)

        # Обработка отдельных параметров книги
        if 'name' in off:
            offer.name = off['name']
            offer.query_data = offer.name.lower()
        if 'author' in off:
            offer.author = off['author']
            offer.query_data += ' ' + offer.author.lower()
        if 'picture' in off:
            offer.picture = off['picture']
        if 'url' in off:
            offer.url = off['url']
        if 'price' in off:
            offer.price = off['price']
        if 'description' in off:
            offer.description = off['description']

        if '@available' in off:
            avail = (off['@available'] == 'true')
            offer.available = avail

        if '@id' in off:
            offer.local_id = int(off['@id'])
        offer.visible = True
        offer.save()

    update_hash_data.value = local_hash
    update_hash_data.save()
    return True


# Автообновление базы данных
def handle_update():
    today = time.strftime("%Y-%m-%d")
    try:
        update_settings = Settings.objects.get(parameter=UPDATE_DATE_PARAMETER)
    except Settings.DoesNotExist:
        update_settings = Settings.objects.create(parameter=UPDATE_DATE_PARAMETER, value='')

    if today != update_settings.value:
        if sync_db():
            update_settings.value = today
            update_settings.save()
